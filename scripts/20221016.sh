sudo apt-get update && sudo apt-get upgrade -y
sudo apt -y install mc
timedatectl set-timezone Europe/Moscow
sudo apt  install docker.io -y
sudo groupadd docker
sudo usermod -aG docker vagrant
sudo systemctl enable docker.service
sudo systemctl enable containerd.service
sudo apt  install docker-compose -y
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
# sudo apt-get update
# sudo apt-get install gitlab-runner -y
sudo usermod -aG docker gitlab-runner
# Установка реджистри и авторизации + серты
sudo iptables -I INPUT -p tcp --dport 5000 -j ACCEPT
sudo netfilter-persistent save
# Путь к репозиторию
mkdir docker
cd docker
mkdir dockerrepo
# Директория для сертификатов
sudo mkdir -p /etc/ssl/docrepo
# Генерируем
sudo openssl req -new -x509 -days 1461 -nodes -out /etc/ssl/docrepo/public.pem -keyout /etc/ssl/docrepo/private.key -subj "/C=RU/ST=Vrn/L=Vrn/O=Global Security/OU=IT Department/CN=192.168.0.180"
sudo -u vagrant mkdir /home/vagrant/auth
sudo -u vagrant docker run --entrypoint htpasswd httpd:2 -Bbn test 123 > /home/vagrant/docker/auth/htpasswd
sudo chown vagrant:vagrant /home/vagrant/docker/auth/htpasswd
# Apache2
sudo apt install apache2 -y
sudo ufw allow "Apache Full"
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/apache-selfsigned.key -out /etc/ssl/certs/apache-selfsigned.crt -subj "/C=RU/ST=Vrn/L=Vrn/O=Global Security/OU=IT Department/CN=192.168.0.180"
sudo cp /etc/apache2/sites-available/default-ssl.conf /etc/apache2/sites-available/default-ssl.conf.bak
cat > /etc/apache2/conf-available/ssl-params.conf <<EOF
SSLCipherSuite EECDH+AESGCM:EDH+AESGCM:AES256+EECDH:AES256+EDH
SSLProtocol All -SSLv2 -SSLv3 -TLSv1 -TLSv1.1
SSLHonorCipherOrder On
# Disable preloading HSTS for now.  You can use the commented out header line that includes
# the "preload" directive if you understand the implications.
# Header always set Strict-Transport-Security "max-age=63072000; includeSubDomains; preload"
Header always set X-Frame-Options DENY
Header always set X-Content-Type-Options nosniff
# Requires Apache >= 2.4
SSLCompression off
SSLUseStapling on
SSLStaplingCache "shmcb:logs/stapling-cache(150000)"
# Requires Apache >= 2.4.11
SSLSessionTickets Off
EOF

sudo rm /etc/apache2/sites-available/default-ssl.conf
cat > /etc/apache2/sites-available/default-ssl.conf <<EOF
<VirtualHost *:443>
        ServerAdmin your_email@example.com
        ServerName 192.168.0.180

        SSLEngine on
        SSLCertificateFile /etc/ssl/certs/apache-selfsigned.crt
        SSLCertificateKeyFile /etc/ssl/private/apache-selfsigned.key

        Header set Host "registry.example.com"
        RequestHeader set X-Forwarded-Proto "https"

        ProxyRequests off
        ProxyPreserveHost on

        # Some HTTPd came configured with /error for error messages
        # In this case, you should disable proxying to remote docker-registry
        # ProxyPass /error/ !
        
        ProxyPass / http://localhost:5000/v2
        ProxyPassReverse / http://localhost:5000/v2

        ErrorLog ${APACHE_LOG_DIR}/registry-error.log
        LogLevel warn
        CustomLog ${APACHE_LOG_DIR}/registry-access.log combined

        <Location />
                Order deny,allow
                Allow from all

               # AuthName "Registry Authentication"
               # AuthType basic
               # AuthUserFile "/etc/apache2/htpasswd/registry-htpasswd"
               # Require valid-user
        </Location>

        # Allow ping and users to run unauthenticated.
        <Location /v1/_ping>
                Satisfy any
                Allow from all
        </Location>

        # Allow ping and users to run unauthenticated.
        <Location /_ping>
               Satisfy any
               Allow from all
        </Location>

</VirtualHost>
EOF
sudo rm /etc/apache2/sites-available/000-default.conf
cat > /etc/apache2/sites-available/000-default.conf <<EOF
<VirtualHost *:80>
        ServerName 192.168.0.180
        Redirect "/" "https://192.168.0.180"
        ServerAdmin webmaster@localhost
        DocumentRoot /var/www/html
        ErrorLog ${APACHE_LOG_DIR}/error.log
        CustomLog ${APACHE_LOG_DIR}/access.log combined
</VirtualHost>
EOF
sudo ufw allow 'Apache Full'
sudo a2enmod ssl
sudo a2enmod headers
sudo a2ensite default-ssl
sudo a2enconf ssl-params
sudo a2enmod proxy_http
sudo systemctl restart apache2
# vim: syntax=apache ts=4 sw=4 sts=4 sr noet

cat <<EOF > docker-compose.yml
#nginx:
#  image: "nginx:alpine"
#  ports:
#    - 5043:443
#  links:
#    - registry:registry
#  volumes:
#    - ./auth:/etc/nginx/conf.d
#    - ./auth/nginx.conf:/etc/nginx/nginx.conf:ro
#    - /etc/ssl/docrepo:/certs
version: "3"
services:
  registry:
    image: registry:2
    container_name: registry
    hostname: registry
    restart: unless-stopped
    environment:
      TZ: "Europe/Moscow"
    ports:
      - 5000:5000
    environment:
      REGISTRY_HTTP_TLS_CERTIFICATE: /certs/public.pem
      REGISTRY_HTTP_TLS_KEY: /certs/private.key
      REGISTRY_AUTH: htpasswd
      REGISTRY_AUTH_HTPASSWD_PATH: /auth/htpasswd
      REGISTRY_AUTH_HTPASSWD_REALM: Registry Realm
    volumes:
      - ./dockerrepo:/var/lib/registry
      - ./etc/ssl/docrepo:/certs
      - ./auth:/auth
EOF
# mkdir -p auth data

docker-compose up -d
