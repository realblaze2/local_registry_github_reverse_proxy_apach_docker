Задача для обучения из "Задачника"

Ссылки на описание "Задачника"

1. https://boosty.to/deusops
2. https://deusops.com/classbook

В текущей задаче надо:
1. Создать ВМ и запустить в ней контейнер "registry:2" (done)
2. Настроить Basic-auth используя образ "httpd:2" (done)
3. Создать docker-compose.yml для запуска этого контейнера с пробросом портов, путей к хранилищу, секретов auth. (Done) 
4. Настроить фронтенд для реджистри (To DO)
5. Автоматизировать создание с помощью Vagrant (Done exept p.4)
6. Научить гитлаб-CI складывать образы в этот реестр (To DO)
(Мысли по 6 пункту: https://www.digitalocean.com/community/tutorials/how-to-build-docker-images-and-host-a-docker-image-repository-with-gitlab)


p.s.
Создание и запуск docker-compose.yml и все остальное - в папке скриптов.


---

### :hammer_and_wrench: Languages and Tools :
<div>
  <img src="https://www.docker.com/wp-content/uploads/2022/03/Docker-Logo-White-RGB_Vertical.png" title="Docker" alt="Docker" width="40" height="40"/>&nbsp;
  <img src="https://icons.iconarchive.com/icons/froyoshark/enkel/128/iTerm-icon.png" title="Bash" alt="Bash" width="40" height="40"/>&nbsp;
  <img src="https://upload.wikimedia.org/wikipedia/commons/4/45/Apache_HTTP_server_logo_%282016%29.png" title="Apache" alt="Apache" width="90" height="40"/>&nbsp;
  <img src="https://icons.iconarchive.com/icons/tatice/operating-systems/128/Linux-icon.png" title="Linux" alt="Linux" width="40" height="40"/>&nbsp;
  <img src=" https://upload.wikimedia.org/wikipedia/commons/8/87/Vagrant.png" title="Vagrant" alt="Vagrant" width="40" height="40"/>&nbsp;
 </div>
